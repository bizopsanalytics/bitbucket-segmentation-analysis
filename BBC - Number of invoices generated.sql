--case study for number of invoices generated in a month

with bitbucket_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.email         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.dim_contact as e on a.contact_id = e.contact_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
        inv_counter as
        (
                select email_domain,
                       month, 
                       count(distinct invoice) as inv_count
                from public.sale
                where financial_year = 'FY2016'
                group by 1,2
        )
        
        select  distinct a.smart_domain, 
                b.month,
                inv_count
        from    bitbucket_paid as a
        left join inv_counter as b on a.smart_domain = b.email_domain
        where month in ('2016-06')
        order by 3 desc
    
        ;
        
        --total invoices generated in a month
 with bitbucket_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.email         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.dim_contact as e on a.contact_id = e.contact_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
        inv_counter as
        (
                select email_domain,
                       month, 
                       count(distinct invoice) as inv_count
                from public.sale
                where financial_year = 'FY2016'
                group by 1,2
        )       
select  b.month,
        count(distinct b.email_domain) as cust_count,
        count(b.invoice) as inv_count       
from    bitbucket_paid as a
left join public.sale as b on a.smart_domain = b.email_domain
where   b.financial_year = 'FY2016'
group by 1
order by 1
