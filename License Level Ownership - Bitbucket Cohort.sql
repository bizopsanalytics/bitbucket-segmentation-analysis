        --license level ownership    
        --fact license active method
      
        with bbc_full as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        and     c.level = 'Full'      
        ),
        bbc_starter as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        and     c.level = 'Starter' 
        ),
        bbc_free as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        and     c.level = 'Free' 
        )
       --Starter,Full and Free
        select   count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bbc_free
        where   customer_id in (select customer_id from bbc_full)
        and     customer_id in (select customer_id from bbc_starter)
        
        --Starter and Free
        select   count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bbc_starter
        where   customer_id in (select customer_id from bbc_free)
        and     customer_id not in (select customer_id from bbc_full)
        
        --Full and Free
        select   count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bbc_full
        where   customer_id in (select customer_id from bbc_free)
        and     customer_id not in (select customer_id from bbc_starter)
  
        
        --Full and Starter
        select   count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bbc_full
        where   customer_id in (select customer_id from bbc_starter)
        and     customer_id not in (select customer_id from bbc_free)
        
        --free only
        select   count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bbc_free
        where   customer_id not in (select customer_id from bbc_starter)
        and     customer_id not in (select customer_id from bbc_full)
        
        --starter only
        select   count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bbc_starter
        where   customer_id not in (select customer_id from bbc_free)
        and     customer_id not in (select customer_id from bbc_full)
        
        --full only
        select   count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bbc_full
        where   customer_id not in (select customer_id from bbc_free)
        and     customer_id not in (select customer_id from bbc_starter)