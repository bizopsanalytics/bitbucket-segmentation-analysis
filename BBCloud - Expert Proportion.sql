select   case 
                        when partner_id = 0 then 0
                        when partner_id > 0 then 1
                        else 2
                end as partner,
                month,  
                hosted_annual,
                base_product,
                count(distinct email_domain) as customer_count,
                count(distinct sen) as sen_count, 
                sum(amount) as FY16_sales,
                sum(partner_discount_amount) as expert_disc
       from     public.sale
       where   base_product = 'Bitbucket'
       and     platform = 'Cloud'
       and financial_year in ('FY2016','FY2017')
       group by 1,2,3,4
       order by 1,2,3,4
       ;
     
      select * from public.sale limit 10