       --MAU
with base as (
select
month
,cast(date_parse(utc_date,'%Y-%m-%d') as date) as utc_date
,product
,parent_sen
,active_users
,new_users
,returning_users
,resurrected_users
,dormant_users
from raw_product.active_instances as a
join model.dim_date as b on (a.utc_date = date_format(b.date_value, '%Y-%m-%d'))
where 1=1
and b.last_day_of_month
and b.calendar_year_month between '2012-01' and '2016-12'
and a.metric = 'mau'
and a.product = 'bitbucket'
)
, instances as (
select
a.parent_sen
,cast(date_parse(min(eval_start_date), '%Y-%m-%d') as date) as initial_start_date
from public.license as a
join base as b using (parent_sen)
where a.base_product = 'Bitbucket'
and a.platform = 'Cloud'
group by 1
 
)
select
a.month
,a.utc_date
,a.product
,sum(a.active_users) as active_users
,sum(a.new_users) as new_users
,sum(case when date_diff('day', b.initial_start_date, a.utc_date) > 28 then a.new_users else 0 end) as n2e_users
,sum(case when b.initial_start_date is null or date_diff('day', b.initial_start_date, a.utc_date) <= 28 then a.new_users else 0 end) as n2n_users
,sum(a.returning_users) as returning_users
,sum(a.resurrected_users) as resurrected_users
,sum(a.dormant_users) as dormant_users
from base as a
left join instances as b using (parent_sen)
group by 1,2,3
order by 1,2,3