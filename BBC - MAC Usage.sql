with all_cust
as
(
select
a.customer_id,
a.contact_id,
a.license_id,
c.sen,
d.smart_domain,
ct.bitbucket_user_ref
from model.fact_license_active as a
join model.dim_product as b on a.product_id = b.product_id
join model.dim_license as c on a.license_id = c.license_id
join model.dim_customer as d on a.customer_id = d.customer_id
join model.dim_contact as ct on ct.contact_id = a.contact_id
where a.date_id = 20160901
and c.level in ('Full', 'Starter')
and b.base_product = 'Bitbucket'
and b.platform = 'Cloud'
--limit 100
),
grouped as
(
select c.customer_id,
c.contact_id,
c.license_id,
c.sen,
c.smart_domain,
count(1) as visit_count
from raw_segment.all_segment_pages p
inner join raw_product.bitbucket_anonymous_id_mapping m on m.anonymous_id = p.anonymousid
inner join all_cust c on c.bitbucket_user_ref = m.id
where dt >= '2016-09-01'
and source = 'my.atlassian.com'
group by 1, 2, 3, 4, 5

)

select  count(distinct customer_id),
        count(distinct contact_id), 
        sum(visit_count) as total_visits
from grouped 