-- how many customers own more than one cloud product?
with bitbucket_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.email         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.dim_contact as e on a.contact_id = e.contact_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.email         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.dim_contact as e on a.contact_id = e.contact_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        and     b.base_product not in ('Marketplace Addon')
        ),
        products as
        (
                select customer_id, count(distinct base_product) as prod_count
                from all_paid
                group by 1
        )
        select  count(distinct a.customer_id) as customer_count,
                count(distinct a.email) as  contact_count          
        from  bitbucket_paid as a
        left join products as b on a.customer_id = b.customer_id
        where b.prod_count > 1
        
      
