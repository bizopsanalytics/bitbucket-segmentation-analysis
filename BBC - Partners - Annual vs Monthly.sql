-- high level

       with 
       all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.partner_id 
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.fact_sale as e on c.sen = e.sen
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'
        and     b.platform = 'Cloud'      

        )
       
        select  case 
                        when partner_id = 0 then 0
                        when partner_id > 0 then 1
                        else 2
                end as partner_status,
                billing_period,
                count(distinct a.smart_domain) as customer_count,
                count(distinct a.sen) as sen_count
        from all_paid as a     
        group by 1,2
        
        
        