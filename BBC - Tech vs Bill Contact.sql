with bitbucket_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        )
select  count(distinct a.customer_id) as customers
from bitbucket_paid as a
left join public.license as b on a.smart_domain = b.tech_email_domain
where   b.base_product = 'Bitbucket'
and     b.platform = 'Cloud'
and     b.tech_contact_id <> bill_contact_id
and     b.last_paid_license_level in ('Full','Starter')

;



