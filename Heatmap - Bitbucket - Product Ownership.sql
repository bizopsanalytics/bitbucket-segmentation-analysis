-- what other products to bitbucket cloud cohort own?
with bitbucket_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.email         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.dim_contact as e on a.contact_id = e.contact_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
        
        select  platform, 
                base_product,
                count(distinct customer_id) as customer_count,
                count(distinct email) as  contact_count
        from    all_paid
        where   customer_id in (select customer_id from bitbucket_paid)
        group by 1,2
        ;
        
        
        
-- what is the bitbucket cloud cohort worth in terms of bookings in FY16?
with bitbucket_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.email         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.dim_contact as e on a.contact_id = e.contact_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
       ,
       finalgroup as
       (
        select  a.customer_id,
                b.platform, 
                b.base_product,
                sum(c.amount) as total_sales
        from    bitbucket_paid as a
        left join all_paid as b on a.customer_id = b.customer_id
        left join public.sale as c on a.smart_domain = c.email_domain and a.base_product = c.base_product and a.platform = c.platform
        where  c.financial_year = 'FY2016'
        --and c.sold_via_partner = true
        group by 1,2,3
        )
        
        select sum(total_sales)
        from finalgroup
        ;
        
         select b.platform, b.base_product, a.smart_domain, sum(amount)
        from bitbucket_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        where  b.financial_year = 'FY2016'
        group by 1,2,3;
        
        ;
        select sum(amount)
        from public.sale
        where financial_year = 'FY2016'
 