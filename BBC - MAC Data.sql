with quote as
(
select count(distinct id)
from raw_segment.all_segment_pages p
inner join raw_product.bitbucket_anonymous_id_mapping m on m.anonymous_id = p.anonymousid
where dt between '2016-08-01' and '2016-09-01'  
and source = 'my.atlassian.com'
--and context_page_path like '%billing/quotes%'
--and
),
checkout as
(
select id
from raw_segment.all_segment_pages p
inner join raw_product.bitbucket_anonymous_id_mapping m on m.anonymous_id = p.anonymousid
where dt between '2016-08-01' and '2016-09-01'
and source = 'my.atlassian.com'
and context_page_path like '%checkout%'
)
select count(distinct id) 
from quote 
where id not in (select id from checkout)

;

select *
from raw_segment.all_segment_pages p
inner join raw_product.bitbucket_anonymous_id_mapping m on m.anonymous_id = p.anonymousid
where dt between '2016-08-01' and '2016-09-01'
and source = 'my.atlassian.com'
--and anonymousid = '3fce20e5-bc95-4d94-869f-9c3dd0306d3c'
limit 40

;

select *
from raw_segment.all_segment_pages
limit 10