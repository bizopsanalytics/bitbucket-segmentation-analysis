-- sen level

with min_month as
(
select email_domain, sen, min(date) as min_date
from public.sale
where base_product = 'Bitbucket'
and platform = 'Cloud'
and hosted_monthly = true
group by 1,2
),
min_annual as
(
select email_domain, sen, min(date) as min_date
from public.sale
where base_product = 'Bitbucket'
and platform = 'Cloud'
and hosted_monthly = false
group by 1,2
),
hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.partner_id 
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.fact_sale as e on c.sen = e.sen
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'
        and     b.platform = 'Cloud'      
        )
select a.email_domain, a.sen--, c.partner_id
from min_annual as a
left join min_month as b on a.sen = b.sen
left join hc_paid as c on a.sen = c.sen
where a.sen in (select sen from hc_paid)
and a.min_date > b.min_date
group by 1,2--,3

;
-- emnail domain
with min_month as
(
select email_domain, sen, min(date) as min_date
from public.sale
where base_product = 'Bitbucket'
and platform = 'Cloud'
and hosted_monthly = true
group by 1,2
),
min_annual as
(
select email_domain, sen, min(date) as min_date
from public.sale
where base_product = 'Bitbucket'
and platform = 'Cloud'
and hosted_monthly = false
group by 1,2
),
hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                e.partner_id 
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        join    model.fact_sale as e on c.sen = e.sen
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'
        and     b.platform = 'Cloud'      
        )
select a.email_domain, a.sen--, c.partner_id
from min_annual as a
left join min_month as b on a.email_domain = b.email_domain
left join hc_paid as c on a.sen = c.sen
where a.email_domain in (select smart_domain from hc_paid)
and a.min_date > b.min_date
group by 1,2--,3