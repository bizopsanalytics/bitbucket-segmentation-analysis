 --bitbucket cloud high-level profile       
        
        select  case 
                        when company_size > 1000 then 9999
                        when company_size is null then 0
                        else company_size
                end as company_size,
                count(distinct a.tech_email_domain) as domain_count,       
                count(distinct a.sen) as sen_count,
                count(distinct a.tech_email) as tech_email_count,
                count(distinct a.bill_email) as billing_email_count
        from    public.license as a
        left join zone_bizops.customer_size as b on a.tech_email_domain = b.email_domain
        where   a.paid_license_end_date > '2016-08-31'
        and     a.base_product = 'Bitbucket'
        and     a.platform = 'Cloud'
        group by 1
        order by 1