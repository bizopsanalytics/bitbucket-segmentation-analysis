select  
                tech_email_domain,
                --tech_country,
                count(distinct sen) as sen_count,
                count(distinct bill_email) as billing_email_count,
                count(distinct tech_email) as tech_email_count,
                count(distinct hosted_url) as hosted_url_count
        from    public.license
        where   paid_license_end_date > '2016-08-31'
        and     base_product = 'Bitbucket'
        and     platform = 'Cloud'
        and     last_license_level <> 'Free'
        and     tech_email_domain = 'accenture.com'
        group by 1--,2
