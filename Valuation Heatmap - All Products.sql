-- value of cloud bookings
with bookings as 
(
        select  email_domain,
                financial_year,  
                sum(amount) as amount
        from    public.sale
        where   financial_year = 'FY2016'
        and     platform = 'Cloud'
        group by 1,2
),
all_paid as
        (
        select  a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')     
        and     b.platform = 'Cloud'  
        ),
        final_cohort as
        (
        select  distinct a.smart_domain
        from    all_paid as a
        )
        select  sum(b.amount), 
                count(distinct smart_domain)
        from final_cohort as a
        left join bookings as b on a.smart_domain = b.email_domain
        
        ;
        
--jira ownership
with bookings as 
(
        select  email_domain,
                financial_year,  
                sum(amount) as amount
        from    public.sale
        where   financial_year = 'FY2016'
        and     platform = 'Cloud'
        group by 1,2
),
 jira_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'JIRA Software'  
        and     b.platform = 'Cloud'
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter') 
        and     b.platform = 'Cloud' 
        )
       
        select  a.base_product,
                count(distinct a.customer_id) as customer_count,
                sum(b.amount) as total_sales
        from    all_paid as a
        left join bookings as b on a.smart_domain = b.email_domain
        where   customer_id in (select customer_id from jira_paid)
        group by 1
        
        ;
        
--confluence ownership
with bookings as 
(
        select  email_domain,
                financial_year,  
                sum(amount) as amount
        from    public.sale
        where   financial_year = 'FY2016'
        and     platform = 'Cloud'
        group by 1,2
), confluence_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Confluence' 
        and     b.platform = 'Cloud' 
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        )
        
      select  a.base_product,
                count(distinct a.customer_id) as customer_count,
                sum(b.amount) as total_sales
        from    all_paid as a
        left join bookings as b on a.smart_domain = b.email_domain
        where   customer_id in (select customer_id from confluence_paid)
        group by 1
        
        ;
        
        --bitbucket ownership
with bookings as 
(
        select  email_domain,
                financial_year,  
                sum(amount) as amount
        from    public.sale
        where   financial_year = 'FY2016'
        and     platform = 'Cloud'
        group by 1,2
),
 bitbucket_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        )
        
          select  a.base_product,
                count(distinct a.customer_id) as customer_count,
                sum(b.amount) as total_sales
        from    all_paid as a
        left join bookings as b on a.smart_domain = b.email_domain
        where   customer_id in (select customer_id from bitbucket_paid)
        group by 1
        
        ;
        
--JSD ownership
with bookings as 
(
        select  email_domain,
                financial_year,  
                sum(amount) as amount
        from    public.sale
        where   financial_year = 'FY2016'
        and     platform = 'Cloud'
        group by 1,2
),
 JSD_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'JIRA Service Desk'  
        and     b.platform = 'Cloud'
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        )
        
         select  a.base_product,
                count(distinct a.customer_id) as customer_count,
                sum(b.amount) as total_sales
        from    all_paid as a
        left join bookings as b on a.smart_domain = b.email_domain
        where   customer_id in (select customer_id from JSD_paid)
        group by 1
        
        ;
        
--Bamboo ownership
with bamboo_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bamboo'  
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
        
        select  base_product,
                count(distinct customer_id)
        from    all_paid
        where   customer_id in (select customer_id from bamboo_paid)
        group by 1
        
        ;
        
--JIRA ownership
with jira_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'JIRA'  
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
        
        select  base_product,
                count(distinct customer_id)
        from    all_paid
        where   customer_id in (select customer_id from jira_paid)
        group by 1
        
        ;
     --hc ownership 
        with hc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
        
        select  base_product,
                count(distinct customer_id)
        from    all_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        
        ;
        
 --fisheye ownership 
        with fish_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'FishEye'  
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
        
        select  base_product,
                count(distinct customer_id)
        from    all_paid
        where   customer_id in (select customer_id from fish_paid)
        group by 1
        ;
        
         --crucible ownership 
        with cruc_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Crucible'  
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
        
        select  base_product,
                count(distinct customer_id)
        from    all_paid
        where   customer_id in (select customer_id from cruc_paid)
        group by 1
        ;
        
            --crucible ownership 
        with core_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'JIRA Core'  
        ),
        all_paid as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        )
        
        select  base_product,
                count(distinct customer_id)
        from    all_paid
        where   customer_id in (select customer_id from core_paid)
        group by 1