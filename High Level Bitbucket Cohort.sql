        --bitbucket cloud high-level profile   
        --license table method.    
        --paid cohort
        select  
                count(distinct tech_email_domain) as domain_count,       
                count(distinct sen) as sen_count,
                count(distinct tech_email) as tech_email_count,
                count(distinct bill_email) as billing_email_count,
                count(distinct hosted_url) as hosted_url_count
        from    public.license
        where   paid_license_end_date > '2016-08-31'
        and     base_product = 'Bitbucket'
        and     platform = 'Cloud'
        and     last_license_level <> 'Free'

       
       ;
        --free cohort
        select  
                count(distinct tech_email_domain) as domain_count,       
                count(distinct sen) as sen_count,
                count(distinct tech_email) as tech_email_count,
                count(distinct bill_email) as billing_email_count,
                count(distinct hosted_url) as hosted_url_count
        from    public.license
        where   base_product = 'Bitbucket'
        and     platform = 'Cloud'
        and     last_license_level = 'Free'

        
        
        ;
        --fact license active method
        --paid
        with bitbucket_cloud_cohort as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        )
        select  
                count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bitbucket_cloud_cohort
        where   level <> 'Free'
;
        
        --fact license active method
        --free
        with bitbucket_cloud_cohort as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        )
        select  
                count(distinct smart_domain) as email_domain_count,
                count(distinct contact_id) as contact_id_count,
                count(distinct license_id) as license_id_count
        from    bitbucket_cloud_cohort
        where   level = 'Free'
        
        ;
        
        -- identify the number of bitbucket customers of each type by ID
 select  license_type, 
         count(distinct id)
 from bitbucket_account
 group by 1
 
 ;
 -- identify the number of bitbucket customers of each type by Email Domain
 select  license_type, 
         count(distinct tech_email_domain)
 from bitbucket_account
 group by 1
                
                