 -- identify the number of bitbucket customers who logged in in the past year
 select  license_type, 
         case 
                when last_login <= '2015-05-30' then 1
                when last_login <= '2016-06-30' then 2
                when last_login <= '2016-09-30' then 3
                else 4
         end as last_login,
         count(distinct id)
 from bitbucket_account
 group by 1,2