 
 --HC Only owned cohort for heatmap.
 
 with hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.unit_count
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        where   a.product_count in (6,7,8) --toggle for prod count
        )
        select  case 
                when unit_count <= 10 then 0
                when unit_count <= 50 then 1
                when unit_count <= 100 then 2
                when unit_count <= 500 then 3
                when unit_count <= 1000 then 4
                when unit_count > 1000 then 5
                else 6
                end as unit_count, 
                count(distinct customer_id) as customer_count
        from    final_cohort 
        group by 1
        order by 1
        ;
  
   
 --HC Only by company size for heatmap
 
 with bookings as
 (
        select email_domain, 
                sum(amount) as amount
       from public.sale
       where financial_year = 'FY2016'
       group by 1
 ),
 
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain,
                a.product_count
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        --where   a.product_count in (6,7,8) --toggle for prod count
        )
        select  case 
                        when b.company_size in(10) then 10
                        when b.company_size in(50) then 50
                        when b.company_size in(200) then 200
                        when b.company_size in(500) then 500
                        when b.company_size in(1000) then 1000
                        when b.company_size > 1000 then 2000
                        when b.company_size is null then 0
                        else company_size
                end as company_size, 
                product_count,
                count(distinct a.customer_id) as customer_count,
                sum(amount) as sales
        from    final_cohort as a
        left join zone_bizops.customer_size as b on a.smart_domain = b.email_domain
        left join bookings as c on a.smart_domain = c.email_domain
        group by 1,2
        order by 1,2
        ;
        
-- heatmap for time as customer
     with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
       -- where   base_product = 'HipChat'
        --and     platform = 'Cloud'
        group by 1
       ),
        hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        where    a.product_count in (4,5,6,7,8) --toggle for prod count
        
        )
        select  a.smart_domain, 
                min(b.min_date) as land_date
        from    final_cohort as a
        left join first_purchase as b on a.smart_domain = b.email_domain
        group by 1
        order by 1           
        -- excel work needed to identify
   
        
        
      
   --HC Only by geo for heatmap
 
 with hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'HipChat'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160630
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
        product_ownership as
        (--calculate many cloud products are owned per customer.
        select  customer_id,
                count(distinct base_product) as product_count
        from    all_cloud_paid
        where   customer_id in (select customer_id from hc_paid)
        group by 1
        ), final_cohort as 
        (--final cohort of HC Cloud only owned for further analysis
        select  a.customer_id, 
                b.smart_domain
        from    product_ownership as a
        left join hc_paid as b on a.customer_id = b.customer_id
        where   a.product_count in (4,5,6,7,8) --toggle for prod count
        )
        select  c.subregion,
                count(distinct a.customer_id) as customer_count
        from    final_cohort as a
        left join public.license as b on a.smart_domain = b.tech_email_domain
        join model.dim_region as c on b.tech_country = c.country_name
        where b.tech_country <> 'Unknown'
        and   b.base_product = 'HipChat'
        group by 1
        order by 2 desc
        
           