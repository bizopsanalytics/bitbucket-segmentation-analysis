 with 
 hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')   
        and     b.base_product = 'Bitbucket'  
        and     b.platform = 'Cloud'
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat'
                                   )     
        ),
         coown_count as
        ( 
                select  smart_domain,
                        customer_id, 
                        count(distinct base_product) as land_count
                from     all_cloud_paid
                where customer_id in (select customer_id from hc_paid)
                group by 1,2
        ),
        coown_prod as
        (
        select  case 
                        -- 1 product only
                        when a.land_count = 1 and b.base_product in ('HipChat') then 1
                        when a.land_count = 1 and b.base_product in ('Confluence') then 2
                        when a.land_count = 1 and b.base_product in ('JIRA') then 3
                        when a.land_count = 1 and b.base_product in ('JIRA Software') then 4
                        when a.land_count = 1 and b.base_product in ('JIRA Service Desk') then 5
                        when a.land_count = 1 and b.base_product in ('JIRA Core') then 6
                        when a.land_count = 1 and b.base_product in ('Bitbucket') then 7
                        -- Hipchat based 2 combos
                        when a.land_count = 2 and b.base_product in ('HipChat','Confluence') then 8 
                        when a.land_count = 2 and b.base_product in ('HipChat','Bitbucket') then 9
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA') then 10
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Software') then 11
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Core') then 12
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Service Desk') then 13
                        --JIRA family and Confluence only
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Confluence') then 14
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Confluence') then 15
                        when a.land_count = 2 and b.base_product in ('JIRA','Confluence') then 16
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Confluence') then 17
                        -- Hipchat based 3 product combos
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','Confluence') then 18
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA') then 19
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Software') then 20  
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Core') then 21
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk') then 22   
                        --Bitbucket based 3 product combos
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA','Confluence') then 23
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Software','Confluence') then 24
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Core','Confluence') then 25
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Service Desk','Confluence') then 26
                        -- 4 product combos
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA','Confluence') then 27
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Software','Confluence') then 28
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Core','Confluence') then 29
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk','Confluence') then 30                       
                        -- > 4 product combos                 
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','Confluence') then 31
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Software','JIRA Core','Confluence') then 32
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Core','JIRA Service Desk','Confluence') then 33
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk', 'JIRA Software','Confluence') then 34
                        
                else 35
                end as coown_group,
                --a.platform, 
                --a.base_product, 
                a.smart_domain
        from coown_count as a
        left join all_cloud_paid as b on a.smart_domain = b.smart_domain
        
       ), final_bb as
       (
       select smart_domain 
       from coown_prod
       where coown_group = 7
       )
,
 all_cust
as
(
select
a.customer_id,
a.contact_id,
a.license_id,
c.sen,
d.smart_domain,
ct.bitbucket_user_ref
from model.fact_license_active as a
join model.dim_product as b on a.product_id = b.product_id
join model.dim_license as c on a.license_id = c.license_id
join model.dim_customer as d on a.customer_id = d.customer_id
join model.dim_contact as ct on ct.contact_id = a.contact_id
where a.date_id = 20160901
and c.level in ('Full', 'Starter')
and b.base_product = 'Bitbucket'
and b.platform = 'Cloud'
--limit 100
),
grouped as
(
select c.customer_id,
c.contact_id,
c.license_id,
c.sen,
c.smart_domain,
count(1) as visit_count
from raw_segment.all_segment_pages p
inner join raw_product.bitbucket_anonymous_id_mapping m on m.anonymous_id = p.anonymousid
inner join all_cust c on c.bitbucket_user_ref = m.id
where dt >= '2016-09-01'
and source = 'my.atlassian.com'
group by 1, 2, 3, 4, 5

)

select  count(distinct customer_id),
        count(distinct contact_id), 
        sum(visit_count) as total_visits
from grouped 
where smart_domain in (select smart_domain from final_bb)