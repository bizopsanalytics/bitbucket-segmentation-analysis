--full product ownership at the email_domain level

with bbc_full as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        and     c.level = 'Full'      
        ),
product_ownership as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level = 'Full'    
        and     a.customer_id in (select customer_id from bbc_full)  
        )
        select   base_product,
                 platform,
                 case    when platform = 'Server' then count(distinct license_id) 
                        when platform = 'Cloud' then count(distinct license_id) 
                        when platform = 'Data Center' then count(distinct license_id) 
                        else count(distinct license_id) 
                 end as platform
        from     product_ownership
        where    license_id not in (select license_id from bbc_full)  
        group by base_product, platform
        
        ;
        
--full product ownership at the contact_id level

with bbc_full as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        and     c.level = 'Full'      
        ),
product_ownership as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level = 'Full'    
        and     a.customer_id in (select contact_id from bbc_full)  
        )
        select   base_product,
                 platform,
                 case   when platform = 'Server' then count(distinct license_id) 
                        when platform = 'Cloud' then count(distinct license_id) 
                        when platform = 'Data Center' then count(distinct license_id) 
                        else count(distinct license_id) 
                 end as platform
        from     product_ownership
        where    license_id not in (select license_id from bbc_full)  
        group by base_product, platform
        
        ;
        
 --full product ownership at the email_domain level for FREE users

with bbc_free as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901
        and     c.level = 'Free'      
        ),
product_ownership as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level = 'Full'    
        and     a.customer_id in (select customer_id from bbc_free)  
        )
        select   base_product,
                 platform,
                 case    when platform = 'Server' then count(distinct license_id) 
                        when platform = 'Cloud' then count(distinct license_id) 
                        when platform = 'Data Center' then count(distinct license_id) 
                        else count(distinct license_id) 
                 end as platform
        from     product_ownership
        where    license_id not in (select license_id from bbc_free)  
        group by base_product, platform
        
        ;       
        
  --full product ownership at the email_domain level for ALL users
         
        with bbc_all as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.product_id in (366,144,157)
        and     a.date_id = 20160901      
        ),
product_ownership as
        (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160901
        and     c.level = 'Full'    
        and     a.customer_id in (select customer_id from bbc_all)  
        )
        select   base_product,
                 platform,
                 case    when platform = 'Server' then count(distinct license_id) 
                        when platform = 'Cloud' then count(distinct license_id) 
                        when platform = 'Data Center' then count(distinct license_id) 
                        else count(distinct license_id) 
                 end as licenses
        from     product_ownership
        where    license_id not in (select license_id from bbc_all)  
        group by base_product, platform
     