--understand how many email domains will be affected by the migration.

select          count(distinct tech_email_domain), 
        (       
                select  
                count(distinct tech_email_domain) as domain_count       
                from    public.license
                where   paid_license_end_date > '2016-08-31'
                and     base_product = 'Bitbucket'
                and     platform = 'Cloud'
                and     last_license_level <> 'Free'
        )       
                as bb_cloud
from    public.license 
where   paid_license_end_date > '2016-08-31'                 
and     last_paid_license_level in ('Full','Starter')
        
        ;
-- license level ownership of bitbucket cloud 
   with bbown as 
   (
   select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where  a.product_id in (366,144,157)
        and    a.date_id = 20160901
        --and     c.level in ('Starter', 'Full', 'Evaluation',)
        --and     c.level in ('Free')
        )
        select  level, 
                --unit_count, 
                --billing_period,
                count(distinct customer_id) as customer_count,
                count(distinct contact_id) as contact_count,
                count(distinct license_id) as license_count
        from bbown
        group by 1,2
        
     ;
--cloud annual customers - license level
     
        with bbown as 
   (
   select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where  a.product_id in (366,144,157)
        and    a.date_id = 20160901
        --and     c.level in ('Starter', 'Full', 'Evaluation',)
        --and     c.level in ('Free')
        and c.billing_period = 'Annual'
        )
        select  level, 
                --unit_count, 
                --billing_period,
                count(distinct customer_id) as customer_count,
                count(distinct contact_id) as contact_count,
                count(distinct license_id) as license_count
        from bbown
        group by 1,2
        
        ;
        
        --cloud annual customers - expiry date
     
 with bbown as 
   (
   select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where  a.product_id in (366,144,157)
        and    a.date_id = 20160901
        --and     c.level in ('Starter', 'Full', 'Evaluation',)
        --and     c.level in ('Free')
        and c.billing_period = 'Annual'
        )
        select  a.level, 
                b.paid_license_end_date, 
                a.sen
        from bbown as a
        left join public.license as b on a.sen = b.sen

       ;
       
       --expert
       
with bbown as 
   (
   select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where -- a.product_id in (366,144,157)
        --and   
         a.date_id = 20160901
        --and     c.level in ('Starter', 'Full', 'Evaluation',)
        --and     c.level in ('Free')
        --and c.billing_period = 'Annual'
        )
        select  a.level,
                a.billing_period,
                b.partner_id,
                count(distinct a.sen)
        from bbown as a
        left join public.license as b on a.sen = b.sen 
        left join model.dim_partner as c on b.partner_id = c.partner_id
        where   b.base_product = 'Bitbucket'
        and     b.platform = 'Cloud'
        group by 1,2,3
       ;
       
       select   case 
                        when partner_id = 0 then 0
                        when partner_id > 0 then 1
                        else 2
                end as partner,
                month,  
                base_product, 
                count(distinct email_domain) as customer_count,
                count(distinct sen) as sen_count, 
                sum(amount) as FY16_sales,
                sum(partner_discount_amount) as expert_disc
       from public.sale
       where   base_product = 'Bitbucket'
       and     platform = 'Cloud'
       and financial_year in ('FY2016','FY2017')
       group by 1,2,3
       order by 1,2,3
       
      ;
      
      select   case 
                        when partner_id = 0 then 0
                        when partner_id > 0 then 1
                        else 2
                end as partner,
                month,  
                hosted_annual,
                base_product,
                count(distinct email_domain) as customer_count,
                count(distinct sen) as sen_count, 
                sum(amount) as FY16_sales,
                sum(partner_discount_amount) as expert_disc
       from     public.sale
       where   base_product = 'Bitbucket'
       and     platform = 'Cloud'
       and financial_year in ('FY2016','FY2017')
       group by 1,2,3,4
       order by 1,2,3,4
   